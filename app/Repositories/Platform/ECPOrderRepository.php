<?php


namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\ECPOrder;
use App\Repositories\Repository;

class ECPOrderRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(ECPOrder::class);
    }

    /**
     * 取得訂單資料by ID
     *
     * @param integer $order_id 付款方式id
     *
     * @return array
     */
    public function getOrderById($order_id)
    {
        return ECPOrder::where("order_id", $order_id)->first();
    }

    /**
     * 取得訂單資料by TradeNo
     *
     * @param string $tradeNo 訂單編號
     *
     * @return array
     */
    public function getOrderByTradeNo($tradeNo)
    {
        return ECPOrder::where('trade_no', $tradeNo);
    }


    /**
     * 取得全部訂單
     *
     * @return array
     */
    public function getAllOrder()
    {
        return ECPOrder::orderByDesc('created_at')->get();
    }

    /**
     * 取得訂單及細項 by id
     *
     * @param integer $order_id 付款方式id
     *
     * @return array
     */
    public function getOrderwithDetailById($order_id)
    {
        return ECPOrder::select(['user_id', 'order_id', 'trade_no', 'trade_date', 'payments.payment_id',
            'payments.payment_name', 'ecp_trade_no', 'payment_date', 'payment_type_charge_fee',
            'total_amount', 'description', 'rtn_code', 'check_mac_value'])
            ->where("order_id", $order_id)
            ->leftjoin('payments', 'ecp_orders.payment_id', 'payments.payment_id')
            ->with(['ecp_order_details' => function ($query) {
                $query->select(['order_id', 'od_id', 'ecp_order_details.item_id', 'quantity', 'items.item_name', 'items.item_amount', 'items.unit'])
                    ->leftjoin('items', 'ecp_order_details.item_id', 'items.item_id')
                    ->orderBy('ecp_order_details.od_id')->get();
            }])->get();
    }
}
