<?php

namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\TPOrderDetail;
use App\Repositories\Repository;

class TPOrderDetailRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(TPOrderDetail::class);
    }
}
