<?php


namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\Credit;
use App\Repositories\Repository;

class CreditRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Credit::class);
    }

    /**
     * 取得信用卡資料
     *
     * @param integer $credit_id 信用卡id
     *
     * @return array
     */
    public function getCreditById($credit_id)
    {
        return Credit::where('credit_id', $credit_id)->first();
    }

}
