<?php


namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\ECPOrderDetail;
use App\Repositories\Repository;

class ECPOrderDetailRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(ECPOrderDetail::class);
    }
}
