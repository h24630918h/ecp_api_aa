<?php


namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\Request;
use App\Repositories\Repository;

class RequestRespository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Request::class);
    }


    /**
     * 取得請求資料by id
     *
     * @param integer $req_id 請求id
     *
     * @return array
     */
    public function getRequestById($req_id)
    {
        return Request::where("req_id", $req_id)->first();
    }

    /**
     * 取得請求資料by id_array
     *
     * @param array $req_id_array 請求id
     *
     * @return array
     */
    public function getRequestInIdArray($req_id_array)
    {
        return Request::whereIn('req_id', $req_id_array)->get();
    }


    /**
     * 取得全部請求資料
     *
     * @return array
     */
    public function getAllRequest()
    {
        return Request::orderByDesc('created_at')->get();
    }


}
