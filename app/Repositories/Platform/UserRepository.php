<?php

namespace App\Repositories\Platform;

use DB;
use App\User;
use App\Repositories\Repository;

class UserRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(User::class);
    }


    /**
     * 取得會員資料
     *
     * @param string $account 帳號
     *
     * @return array
     */
    public function getUserByAccount($account)
    {
        return User::where('account', $account)->first();
    }


    /**
     * 取得全部會員
     *
     * @return array
     */
    public function getAllUser()
    {
        return User::orderByDesc('created_at')->get();
    }

    /**
     * 取得會員+訂單
     *
     * @param string $account 帳號
     *
     * @return array
     */
    public function getWithOrder($account)
    {
        return User::select('users.user_id', 'users.account as user_account', 'users.name as user_name', 'users.phone', 'users.addr', 'users.email')
            ->where('users.account', $account)
            ->with(['ecp_orders' => function ($query) {
                $query->select(['user_id', 'order_id', 'trade_no', 'trade_date', 'payments.payment_id',
                    'payments.payment_name', 'ecp_trade_no', 'payment_date', 'payment_type_charge_fee',
                    'total_amount', 'description', 'rtn_code', 'check_mac_value'])
                    ->join('payments', 'ecp_orders.payment_id', 'payments.payment_id')
                    ->orderBy('ecp_orders.created_at')->get();
            }])
            ->with(['tp_orders' => function ($query) {
                $query->select(['user_id', 'order_id', 'payments.payment_id', 'payments.payment_name',
                    'currency', 'order_number', 'bank_transaction_id', 'details', 'amount', 'status',
                    'transaction_time', 'rec_trade_id', 'auth_code', 'bank_transaction_start',
                    'bank_transaction_end', 'bank_result_code', 'bank_result_msg', 'payment_url'])
                    ->join('payments', 'tp_orders.payment_id', 'payments.payment_id')
                    ->orderBy('tp_orders.created_at')->get();
            }])->get();
    }


    /**
     * 取得個人信用卡資料
     *
     * @param string $account 帳號
     * @return array
     */
    public function getUserCredits($account)
    {
        return User::select('users.user_id', 'users.account as user_account', 'users.name as user_name', 'users.phone', 'users.addr', 'users.email')
            ->where('users.account', $account)
            ->with(['credits' => function ($query) {
                $query->select(['user_id', 'bin_code', 'last_four', 'issuer', 'issuer_zh_tw',
                    'bank_id', 'funding', 'type', 'level', 'country', 'country_code', 'expiry_date'])
                    ->orderBy('credits.created_at')->get();
            }])->get();
    }


}
