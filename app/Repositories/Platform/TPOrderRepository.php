<?php


namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\TPOrder;
use App\Repositories\Repository;

class TPOrderRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(TPOrder::class);
    }

    /**
     * 取得訂單資料by ID
     *
     * @param integer $order_id 訂單id
     *
     * @return array
     */
    public function getOrderById($order_id)
    {
        return TPOrder::where('order_id', $order_id)->first();
    }

    /**
     * 取得訂單資料by orderNumber
     *
     * @param string $orderNumber 訂單編號
     *
     * @return array
     */
    public function getOrderByOrderNumber($orderNumber)
    {
        return TPOrder::where('order_number', $orderNumber)->first();
    }


    /**
     * 取得全部訂單
     *
     * @return array
     */
    public function getAllOrder()
    {
        return TPOrder::orderByDesc('created_at')->get();
    }

    /**
     * 取得訂單及細項 by id
     *
     * @param integer $order_id 付款方式id
     *
     * @return array
     */
    public function getOrderwithDetailById($order_id)
    {
        return TPOrder::select(['user_id', 'order_id', 'payments.payment_id', 'payments.payment_name',
            'currency', 'order_number', 'bank_transaction_id', 'details', 'amount', 'status',
            'transaction_time', 'rec_trade_id', 'auth_code', 'bank_transaction_start',
            'bank_transaction_end', 'bank_result_code', 'bank_result_msg', 'payment_url'])
            ->where("order_id", $order_id)
            ->leftjoin('payments', 'tp_orders.payment_id', 'payments.payment_id')
            ->with(['tp_order_details' => function ($query) {
                $query->select(['order_id', 'od_id', 'tp_order_details.item_id', 'quantity', 'items.item_name', 'items.item_amount', 'items.unit'])
                    ->leftjoin('items', 'tp_order_details.item_id', 'items.item_id')
                    ->orderBy('tp_order_details.od_id')->get();
            }])->get();
    }

}
