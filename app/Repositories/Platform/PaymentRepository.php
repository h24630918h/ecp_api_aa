<?php


namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\Payment;
use App\Repositories\Repository;

class PaymentRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Payment::class);
    }

    /**
     * 取得付款方式資料 by id
     *
     * @param integer $payment_id 付款方式id
     *
     * @return array
     */
    public function getPaymentById($payment_id)
    {
        return Payment::where("payment_id", $payment_id)->first();
    }

    /**
     * 取得 ecpay 付款方式資料
     *
     * @param integer $payment_id 付款方式id
     *
     * @return array
     */
    public function getECPPaymentById($payment_id)
    {
        return Payment::where('payment_id', $payment_id)->where('payment_flow', 1)->first();
    }

    /**
     * 取得 tap pay 付款方式資料
     *
     * @param integer $payment_id 付款方式id
     *
     * @return array
     */
    public function getTPPaymentById($payment_id)
    {
        return Payment::where('payment_id', $payment_id)->where('payment_flow', 2)->first();
    }


    /**
     * 取得全部付款方式
     *
     * @return array
     */
    public function getAllPayment()
    {
        return Payment::orderByDesc('created_at')->get();
    }
}
