<?php


namespace App\Repositories\Platform;

use DB;

use App\Entities\Model\Item;
use App\Repositories\Repository;

class ItemRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Item::class);
    }


    /**
     * 取得商品資料by id
     *
     * @param integer $item_id 商品id
     *
     * @return array
     */
    public function getItemById($item_id)
    {
        return Item::where("item_id", $item_id)->first();
    }

    /**
     * 取得商品資料by id_array
     *
     * @param array $item_id_array 商品id
     *
     * @return array
     */
    public function getItemInIdArray($item_id_array)
    {
        return Item::whereIn('item_id', $item_id_array)->get();
    }


    /**
     * 取得全部商品資料
     *
     * @return array
     */
    public function getAllItem()
    {
        return Item::orderByDesc('created_at')->get();
    }


}
