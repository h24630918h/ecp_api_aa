<?php


namespace App\Services;

use App\Repositories\Platform\PaymentRepository;

class PaymentService
{

    private $paymentRepository;

    public function __construct()
    {
        $this->paymentRepository = new PaymentRepository();
    }

    #region  新增付款方式

    /**
     * 新增付款方式
     *
     * @param array $postData
     * @return array
     */
    public function newPayment($postData)
    {
        try {

            $newPayment = $this->paymentRepository->create($postData);
            return array('result' => $newPayment, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 刪除付款方式
    /**
     * 刪除付款方式
     *
     * @param array $postData
     * @return array
     */
    public function deletePayment($postData)
    {
        try {

            if (strpos($postData['user']['account'], 'admin') === false) {
                return array("error" => '您不是管理員，無刪除商品權限', 'code' => config('apiCode.invalidPermission'));
            }

            $deleteData = $this->paymentRepository->getPaymentById($postData['payment_id']);

            if (!$deleteData) {
                return array("error" => '查無此資料', 'code' => config('apiCode.notFound'));
            }

            $deleteData->delete();
            return array("result" => 1, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得全部付款方式
    /**
     * 取得全部付款方式
     *
     * @return array
     */
    public function getAllPayment()
    {
        try {

            $getPaymentData = $this->paymentRepository->getAllPayment();
            if ($getPaymentData->first())
                return array("result" => $getPaymentData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無此資料", 'code' => config('apiCode.notFound'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得付款方式 by id
    /**
     * 取得付款方式 by id
     *
     * @param array $postData
     * @return array
     */
    public function getPaymentById($postData)
    {
        try {

            $getPaymentData = $this->paymentRepository->getPaymentById($postData['payment_id']);

            if ($getPaymentData)
                return array("result" => $getPaymentData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無此資料", 'code' => config('apiCode.notFound'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion
}
