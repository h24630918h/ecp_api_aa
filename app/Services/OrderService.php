<?php

namespace App\Services;

use App\Repositories\Platform\ECPOrderRepository;
use App\Repositories\Platform\ECPOrderDetailRepository;
use App\Repositories\Platform\TPOrderRepository;
use App\Repositories\Platform\TPOrderDetailRepository;

use App\Repositories\Platform\PaymentRepository;
use App\Repositories\Platform\ItemRepository;
use App\Repositories\Platform\CreditRepository;
use GuzzleHttp\Client;
use \ECPay_AllInOne as ECPay;
use DB;

class OrderService
{
    use BaseService;

    private $ecpOrderRepository;
    private $ecpOrderDetailRepository;
    private $tpOrderRepository;
    private $tpOrderDetailRepository;

    private $paymentRepository;
    private $itemRepository;
    private $creditRepository;
    private $client;

    public function __construct()
    {
        $this->ecpOrderRepository = new ECPOrderRepository();
        $this->ecpOrderDetailRepository = new ECPOrderDetailRepository();
        $this->tpOrderRepository = new TPOrderRepository();
        $this->tpOrderDetailRepository = new TPOrderDetailRepository();

        $this->paymentRepository = new PaymentRepository();
        $this->itemRepository = new   ItemRepository();
        $this->creditRepository = new CreditRepository();
        $this->client = new Client();
    }

    #region  設定ECPay物件參數

    /**
     * 設定ECPay物件參數
     *
     * @param ECPay $obj ECPay物件
     * @param string $url api網址
     * @param array $querry 查詢物件
     * @param array $send 創建物件
     */
    public function setECPay(ECPay $obj, $url, $querry = array(), $send = array())
    {
        $obj->ServiceURL = \config('ECP_Config.ECP_Base_Uri') . $url;    //服務位置
        $obj->HashKey = \config('ECP_Config.ECP_HashKey');                                       //測試用Hashkey，請自行帶入ECPay提供的HashKey
        $obj->HashIV = \config('ECP_Config.ECP_HashIV');                                         //測試用HashIV，請自行帶入ECPay提供的HashIV
        $obj->MerchantID = \config('ECP_Config.ECP_MerchantID');                                 //測試用MerchantID，請自行帶入ECPay提供的MerchantID
        $obj->EncryptType = 1;                                                                        //CheckMacValue加密類型，請固定填入1，使用SHA256加密

        foreach ($querry as $key => $val) {
            $obj->Query[$key] = $val;
        }

        foreach ($send as $key => $val) {
            $obj->Send[$key] = $val;
        }
    }
    #endregion

    #region order_detail前處理
    /**
     * order_detail前處理
     *
     * @param array $odArray order_detail陣列
     * @return array [totalAmount,itemName,getItemData]
     */
    public function odPretreatment($odArray)
    {

        $totalAmount = 0;
        $itemName = "";
        //將order_detail[]內的 item_id取出成為$itemList[]
        $itemList = array_column($odArray, 'item_id');
        $getItemData = $this->itemRepository->getItemInIdArray($itemList);

        //將order_detail[]內的資料取出，並以item_id為key、quantity為value
        $mergeItemList = array_column($odArray, 'quantity', 'item_id');

        //遍歷$getItemData 如果id相同 則把quantity帶入
        //總金額相加
        //產品名稱合併為字串
        foreach ($getItemData as $key => $item) {
            $exist = array_key_exists($item['item_id'], $mergeItemList);
            if ($exist) {
                $getItemData[$key]['quantity'] = $mergeItemList[$item['item_id']];
            }
            $totalAmount += $getItemData[$key]['item_amount'] * $getItemData[$key]['quantity'];
            $itemName .= $getItemData[$key]['item_name'] . $getItemData[$key]['item_amount'] . '元 X' . $getItemData[$key]['quantity'] . '#';
        }

        $result = [
            'totalAmount' => $totalAmount,
            'itemName' => $itemName,
            'getItemData' => $getItemData
        ];

        return $result;
    }
    #endregion

    #region 呼叫tapPayApi

    /**
     * 呼叫tapPayApi
     *
     * @param string $url api網址
     * @param array $tpData tapPay參數
     * @return array
     */
    public function callTapPay($url, $tpData)
    {
        $response = $this->client->post(
            \config('TP_Config.TP_Base_Uri') . $url,
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'x-api-key' => $tpData['partner_key']
                ],
                'json' => $tpData,
            ]);
        $result = json_decode($response->getBody()->getContents(), true);
        return $result;
    }
    #endregion

    #region  新增ecp訂單

    /**
     * 新增ecp訂單
     *
     * @param array $postData
     * @param array $odItemJsonArray order_detail陣列
     * @return array
     */
    public function newECPOrder($postData, $odItemJsonArray)
    {
        try {

            $getPaymentData = $this->paymentRepository->getECPPaymentById($postData['payment_id']);
            if (!$getPaymentData) {
                return array('error' => "查無付款方式資料", 'code' => config('apiCode.notFound'));
            }

            $odPretreatmentData = $this->odPretreatment($odItemJsonArray);

            $postData['total_amount'] = $odPretreatmentData['totalAmount'];
            $postData['trade_no'] = $this->RandomNameFillUp(date("YmdHis", time()), 20);
            $postData['rtn_code'] = 0;
            $postData['user_id'] = $postData['user']['user_id'];
            $postData['trade_date'] = date('Y/m/d H:i:s');

            $sdk_Return = $this->ECP_Create_Order($postData, $odPretreatmentData['getItemData']);
            $postData['check_mac_value'] = $sdk_Return['CheckMacValue'];

            DB::transaction(function () use ($postData, $odItemJsonArray) {
                $newOrder = $this->ecpOrderRepository->create($postData);
                foreach ($odItemJsonArray as $item) {
                    $item['order_id'] = $newOrder->order_id;
                    $this->ecpOrderDetailRepository->create($item);
                }
            });

            $returnUrl = "https://payment-stage.ecpay.com.tw/SP/SPCheckOut?MerchantID="
                . $sdk_Return['MerchantID'] . "&SPToken=" . $sdk_Return['SPToken'] . "&PaymentType=" . $getPaymentData['payment_name'];

            return array('result' => $returnUrl, 'code' => config('apiCode.success'));


        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 串接綠界api

    /**
     * 串接綠界api
     *
     * @param array $postData
     * @param array $getItemData 商品
     * @return array
     */
    public function ECP_Create_Order($postData, $getItemData)
    {
        try {

            $obj = new ECPay;

            //基本參數(請依系統規劃自行調整)
            $send = [
                'ReturnURL' => "http://www.ecpay.com.tw/receive.php",    //付款完成通知回傳的網址
                'MerchantTradeNo' => $postData['trade_no'],                         //訂單編號
                'MerchantTradeDate' => $postData['trade_date'],                       //交易時間
                'TotalAmount' => $postData['total_amount'],                                      //交易金額
                'TradeDesc' => $postData['description'],                          //交易描述
                'ChoosePayment' => 'ALL',              //付款方式:預設all
                'Items' => array(),
            ];

            //訂單的商品資料
            foreach ($getItemData as $item) {
                array_push($send['Items'], array(
                    'Name' => $item['item_name'],
                    'Price' => (int)$item['item_amount'],
                    'Currency' => "元",
                    'Quantity' => (int)$item['quantity'],
                ));
            }

            $this->setECPay($obj, "/SP/CreateTrade", array(), $send);
            $sdk_Return = $obj->CreateTrade();

            //拿到返回參數
            return $sdk_Return;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    #endregion

    #region  接收ecp訂單回應

    /**
     * 接收ecp訂單回應
     *
     * @param array $postData
     * @return array
     */
    public function recieveECPResult($postData)
    {
        try {

            $orderData = $this->ecpOrderRepository->getOrderByTradeNo($postData['TradeNo']);
            if (!$orderData) {
                return array('error' => '查無資料', 'code' => config('apiCode.notFound'));
            }
            if ($postData['MerchantID'] != env('ECP_MerchantID', false)) {
                return array('error' => '特店編號錯誤', 'code' => config('apiCode.validateFail'));
            }
            if ($postData['RtnCode'] != 1) {
                return array('error' => $postData['RtnMsg'], 'code' => config('apiCode.validateFail'));
            }

            $orderData->rtn_code = $postData['RtnCode'];
            $orderData->ecp_trade_no = $postData['TradeNo'];
            $orderData->payment_date = $postData['$OrderData'];
            $orderData->payment_type_charge_fee = $postData['PaymentTypeChargeFee'];
            $orderData->save();
            return array('result' => 1, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region  查詢ECP訂單

    /**
     * 查詢ECP訂單
     *
     * @param array $postData
     * @return array
     */
    public function searchECPOrder($postData)
    {
        try {
            $obj = new ECPay;

            $orderData = $this->ecpOrderRepository->getOrderById($postData['order_id']);

            if (!$orderData) {
                return array('error' => '查無該訂單', 'code' => config('apiCode.notFound'));
            }

            //設定ecpay參數
            $query = [
                'MerchantTradeNo' => $orderData->trade_no, //訂單編號
                'TimeStamp' => date('Y/m/d H:i:s'),
            ];

            $this->setECPay($obj, "/Cashier/QueryTradeInfo/V5", $query);
            $sdk_Return = $obj->QueryTradeInfo();

            return array('result' => $sdk_Return, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得本機ECP訂單 by id
    /**
     * 取得本機ECP訂單 by id
     *
     * @param array $postData
     * @return array
     */
    public function getECPOrderById($postData)
    {
        try {

            $getOrderData = $this->ecpOrderRepository->getOrderwithDetailById($postData['order_id']);

            if ($getOrderData->first())
                return array("result" => $getOrderData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無此資料", 'code' => config('apiCode.notFound'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region  新增 PayByPrime tap pay訂單

    /**
     * 新增 PayByPrime tap pay訂單
     *
     * @param array $postData
     * @param array $odItemJsonArray order_detail陣列
     * @return array
     */
    public function PayByPrimeTpOrder($postData, $odItemJsonArray)
    {
        try {

            $getPaymentData = $this->paymentRepository->getTPPaymentById($postData['payment_id']);
            if (!$getPaymentData)
                return array('error' => "查無付款方式資料", 'code' => config('apiCode.notFound'));

            #region 設定 tapPay api參數
            $odPretreatmentData = $this->odPretreatment($odItemJsonArray);
            $user = $postData['user'];
            $tpData = [
                'partner_key' => \config('TP_Config.TP_PartnerKey'),
                'merchant_id' => \config('TP_Config.TP_MerchantID'),
                'prime' => $postData['prime'],

                'user_id' => $user['user_id'],
                'payment_id' => $postData['payment_id'],
                'details' => $postData['details'],
                'amount' => $odPretreatmentData['totalAmount'],
                'order_number' => 'order' . $this->RandomNameFillUp(date("YmdHis", time()), 20),
                'bank_transaction_id' => 'bank' . $this->RandomNameFillUp(date("YmdHis", time()), 13),
                'transaction_time' => date('Y/m/d H:i:s'),
                'currency' => isset($postData['currency']) ? $postData['currency'] : 'TWD',
                'remember' => ($postData['remember'] == 0) ? false : true,
                'status' => 500,
                'cardholder' => [
                    "phone_number" => $user['phone'],
                    "name" => $user['name'],
                    "email" => $user['email']
                ]
            ];
            $newOrder = $this->tpOrderRepository->create($tpData);
            #endregion

            //  呼叫tapPay api
            $response = $this->callTapPay('payment/pay-by-prime', $tpData);

            if ($response['status'] !== 0) {
                return array('error' => $response['msg'], 'code' => config('apiCode.validateFail'));
            }
            $newOrder->rec_trade_id = $response['rec_trade_id'];
            $newOrder->auth_code = $response['auth_code'];
            $newOrder->bank_result_code = $response['bank_result_code'];
            $newOrder->bank_result_msg = $response['bank_result_msg'];
            $bank_transaction_time = $response['bank_transaction_time'];

            $newOrder->bank_transaction_start = date("Y/m/d H:i:s", ($bank_transaction_time['start_time_millis'] / 1000));
            $newOrder->bank_transaction_end = date("Y/m/d H:i:s", ($bank_transaction_time['end_time_millis'] / 1000));
            $newOrder->status = 0;

            #region 如果有設定rember的話將信用卡整理
            if ($postData['remember']) {
                $card_secret = $response['card_secret'];
                $card_info = $response['card_info'];

                $creditData = [
                    'user_id' => $postData['user']['user_id'],
                    'card_token' => $card_secret['card_token'],
                    'card_key' => $card_secret['card_key'],

                    'bin_code' => $card_info['bin_code'],
                    'last_four' => $card_info['last_four'],
                    'issuer' => $card_info['issuer'],
                    'issuer_zh_tw' => $card_info['issuer_zh_tw'],
                    'bank_id' => $card_info['bank_id'],
                    'funding' => $card_info['funding'],
                    'type' => $card_info['type'],
                    'level' => $card_info['level'],
                    'country' => $card_info['country'],
                    'country_code' => $card_info['country_code'],
                    'expiry_date' => $card_info['expiry_date'],
                ];
            }
            #endregion

            #region 存入資料庫 Orders,OrderDetails,Credits
            DB::transaction(function () use ($tpData, $odItemJsonArray, $creditData, $newOrder) {

                $newOrder->save();
                foreach ($odItemJsonArray as $item) {
                    $item['order_id'] = $newOrder->order_id;
                    $this->tpOrderDetailRepository->create($item);
                }
                $this->creditRepository->create($creditData);
            });
            #endregion

            return array('result' => $response, 'code' => config('apiCode.success'));


        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {

            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 新增 PayByToken tapPay訂單

    /**
     * 新增 PayByToken tapPay訂單
     *
     * @param array $postData
     * @param array $odItemJsonArray order_detail陣列
     * @return array
     */
    public function PayByTokenTpOrder($postData, $odItemJsonArray)
    {

        try {

            $getPaymentData = $this->paymentRepository->getTPPaymentById($postData['payment_id']);
            if (!$getPaymentData)
                return array('error' => "查無付款方式資料", 'code' => config('apiCode.notFound'));

            $getCreditData = $this->creditRepository->getCreditById($postData['credit_id']);
            if (!$getCreditData)
                return array('error' => "查無該信用卡資訊", 'code' => config('apiCode.notFound'));

            #region 設定 tapPay api參數
            $odPretreatmentData = $this->odPretreatment($odItemJsonArray);
            $user = $postData['user'];
            $tpData = [
                'partner_key' => \config('TP_Config.TP_PartnerKey'),
                'merchant_id' => \config('TP_Config.TP_MerchantID'),
                'card_token' => $getCreditData->card_token,
                'card_key' => $getCreditData->card_key,

                'user_id' => $user['user_id'],
                'payment_id' => $postData['payment_id'],
                'details' => $postData['details'],
                'amount' => $odPretreatmentData['totalAmount'],
                'order_number' => 'order' . $this->RandomNameFillUp(date("YmdHis", time()), 20),
                'bank_transaction_id' => 'bank' . $this->RandomNameFillUp(date("YmdHis", time()), 13),
                'transaction_time' => date('Y/m/d H:i:s'),
                'currency' => isset($postData['currency']) ? $postData['currency'] : 'TWD',
                'status' => 500,
            ];
            $newOrder = $this->tpOrderRepository->create($tpData);
            #endregion

            // 呼叫tap pay api
            $response = $this->callTapPay('payment/pay-by-token', $tpData);

            if ($response['status'] !== 0) {
                return array('error' => $response['msg'], 'code' => config('apiCode.validateFail'));
            }

            $newOrder->rec_trade_id = $response['rec_trade_id'];
            $newOrder->auth_code = $response['auth_code'];
            $newOrder->bank_result_code = $response['bank_result_code'];
            $newOrder->bank_result_msg = $response['bank_result_msg'];
            $bank_transaction_time = $response['bank_transaction_time'];

            $newOrder->bank_transaction_start = date("Y/m/d H:i:s", ($bank_transaction_time['start_time_millis'] / 1000));
            $newOrder->bank_transaction_end = date("Y/m/d H:i:s", ($bank_transaction_time['end_time_millis'] / 1000));
            $newOrder->status = 0;

            #region 存入資料庫 Orders,OrderDetails,Credits
            DB::transaction(function () use ($tpData, $odItemJsonArray, $newOrder) {
                $newOrder->save();
                foreach ($odItemJsonArray as $item) {
                    $item['order_id'] = $newOrder->order_id;
                    $this->tpOrderDetailRepository->create($item);
                }
            });
            #endregion

            return array('result' => $response, 'code' => config('apiCode.success'));


        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {

            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }

    #endregion

    //region 查詢TP訂單

    /**
     *  查詢TP訂單
     *
     * @param array $postData
     * @return array
     */
    public function searchTPOrder($postData)
    {
        try {
            $tpData = [
                'partner_key' => \config('TP_Config.TP_PartnerKey'),
                'page' => 0,
                'order_by' => [
                    'attribute' => 'time',
                    'is_descending' => true,
                ],
            ];


            if (isset($postData['order_id'])) {
                $orderData = $this->tpOrderRepository->getOrderById($postData['order_id']);
                if (!$orderData) {
                    return array("error" => "訂單id錯誤", 'code' => config('apiCode.notFound'));
                }
                $tpData['filters']['order_number'] = $orderData->order_number;
            }

            if (isset($postData['start_time'])) {
                $tpData['filters']['time']['start_time'] = $this->milliSeconds($postData['start_time']);
            }
            if (isset($postData['end_time'])) {
                $tpData['filters']['time']['end_time'] = $this->milliSeconds($postData['end_time']);
            }

            if (isset($postData['upper_limit'])) {
                $tpData['filters']['amount']['upper_limit'] = $postData['upper_limit'];
            }
            if (isset($postData['lower_limit'])) {
                $tpData['filters']['amount']['lower_limit'] = $postData['lower_limit'];
            }
            if (isset($postData['page'])) {
                $tpData['page'] = $postData['page'];
            }

            // 呼叫tap pay api
            $result = $this->callTapPay('transaction/query', $tpData);
            return array('result' => $result, 'code' => config('apiCode.success'));


        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {

            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }

    }
    #endregion

    #region 取得本機TP訂單 by id
    /**
     *  取得本機TP訂單 by id
     *
     * @param array $postData
     * @return array
     */
    public function getTPOrderById($postData)
    {
        try {

            $getOrderData = $this->tpOrderRepository->getOrderwithDetailById($postData['order_id']);

            if ($getOrderData->first())
                return array("result" => $getOrderData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無此資料", 'code' => config('apiCode.notFound'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion
}
