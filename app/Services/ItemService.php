<?php


namespace App\Services;

use App\Repositories\Platform\ItemRepository;

class ItemService
{

    private $itemRepository;

    public function __construct()
    {
        $this->itemRepository = new ItemRepository();
    }

    #region  新增商品

    /**
     * 新增商品
     *
     * @param array $postData
     * @return array
     */
    public function newItem($postData)
    {
        try {

            $newItem = $this->itemRepository->create($postData);
            return array('result' => $newItem, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 刪除商品
    /**
     * 刪除商品
     *
     * @param array $postData
     * @return array
     */
    public function deleteItem($postData)
    {
        try {
            if (strpos($postData['user']['account'], 'admin') === false) {
                return array("error" => '您不是管理員，無刪除商品權限', 'code' => config('apiCode.invalidPermission'));
            }

            $deleteData = $this->itemRepository->getItemById($postData['item_id']);

            if (!$deleteData) {
                return array("error" => '查無此資料', 'code' => config('apiCode.notFound'));
            }

            $deleteData->delete();
            return array("result" => 1, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得全部商品
    /**
     * 取得全部商品
     *
     * @return array
     */
    public function getAllItem()
    {
        try {

            $getItemData = $this->itemRepository->getAllItem();
            if ($getItemData->first())
                return array("result" => $getItemData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無此資料", 'code' => config('apiCode.notFound'));


        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得商品by id
    /**
     * 取得商品by id
     *
     * @param array $postData
     * @return array
     */
    public function getItemById($postData)
    {
        try {

            $getItemData = $this->itemRepository->getItemById($postData['item_id']);
            if (isset($getItemData))
                return array("result" => $getItemData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無此資料", 'code' => config('apiCode.notFound'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion
}
