<?php


namespace App\Services;

use App\Repositories\Platform\UserRepository;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;
use Auth;

class UserService
{

    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    #region  會員註冊

    /**
     * 會員註冊
     *
     * @param array $postData
     * @return array
     */
    public function register($postData)
    {
        try {

            $postData['password'] = bcrypt($postData['password']);

            $newUser = $this->userRepository->create($postData);
            return array('result' => array("token" => JWTAuth::fromUser($newUser), "user" => $newUser), 'code' => config('apiCode.success'));


        } catch (JWTException $e) {
            return array('error' => $e->getMessage(), 'code' => $e->getCode() ?? config('apiCode.couldNotCreateToken'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region  會員登入
    /**
     * 會員登入
     *
     * @param array $postData
     * @return array
     */
    public function login($postData)
    {
        try {

            $user = $this->userRepository->getUserByAccount($postData['account']);

            if ($user) {

                if (Hash::check($postData['password'], $user->password)) {
                    return array('result' => array("token" => JWTAuth::fromUser($user), "user" => $user,), 'code' => config('apiCode.success'));

                }
                return array('error' => "密碼錯誤", 'code' => config('apiCode.invalidCredentials'));
            }
            return array('error' => "查無此會員", 'code' => config('apiCode.memberNotFound'));

        } catch (JWTException $e) {
            return array('error' => $e->getMessage(), 'code' => $e->getCode() ?? config('apiCode.couldNotCreateToken'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得全部會員
    /**
     * 取得全部會員
     *
     * @return array
     */
    public function getAllUser()
    {
        try {

            $getUserData = $this->userRepository->getAllUser();
            if ($getUserData->first())
                return array("result" => $getUserData, 'code' => config('apiCode.success'));
            else
                return array("result" => "查無會員", 'code' => config('apiCode.memberNotFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region  取得會員資料 by帳號
    /**
     * 取得會員資料 by帳號
     *
     * @param array $postData
     * @return array
     */
    public function getUserByAccount($postData)
    {
        try {

            $getUserData = $this->userRepository->getUserByAccount($postData['account']);
            if ($getUserData)
                return array("result" => $getUserData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無此會員", 'code' => config('apiCode.memberNotFound'));


        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion


    #region 刪除會員資料
    /**
     * 刪除會員資料
     *
     * @param array $postData
     * @return array
     */
    public function deleteUser($postData)
    {
        try {
            if (strpos($postData['account'], 'admin') !== false) {
                return array("error" => '無法刪除admin', 'code' => config('apiCode.invalidPermission'));
            }

            $deleteData = $this->userRepository->getUserByAccount($postData['account']);

            if (!$deleteData) {
                return array("error" => '查無此會員', 'code' => config('apiCode.memberNotFound'));
            }

            $deleteData->delete();
            return array("result" => 1, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得使用者訂單
    /**
     * 取得使用者訂單
     *
     * @param array $postData
     * @return array
     */
    public function getUserOrder($postData)
    {
        try {

            $getUserOrderData = $this->userRepository->getWithOrder($postData['user']['account']);
            if ($getUserOrderData->first())
                return array("result" => $getUserOrderData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無資料", 'code' => config('apiCode.memberNotFound'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion

    #region 取得個人信用卡資訊
    /**
     * 取得個人信用卡資訊
     *
     * @param array $postData
     * @return array
     */
    public function getUserCredits($postData)
    {
        try {

            $getUesrCreditsData = $this->userRepository->getUserCredits($postData['user']['account']);
            if ($getUesrCreditsData->first())
                return array("result" => $getUesrCreditsData, 'code' => config('apiCode.success'));
            else
                return array("error" => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.notAPICode'));
        } catch (Throwable $t) {

            return array('error' => $t->getMessage(), 'code' => config('apiCode.ServiceUnavailable'));
        }
    }
    #endregion


}

