<?php

namespace App\Http\Middleware;

use App\Repositories\Platform\RequestRespository;
use Illuminate\Support\Facades\Log;
use Closure;

class LogAfterRequest
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $r = new RequestRespository();
        $url = $request->fullUrl();
        $ip = $request->ip();
        $responseData['ip'] = $ip;
        $responseData['url'] = $url;
        $responseData['request'] = json_encode($request->all(),JSON_UNESCAPED_UNICODE);
        $responseData['code'] = $response->getStatusCode();
        $r->create($responseData);


        return $response;
    }

//    public function terminate($request, $response)
//    {
//
//    }

}
