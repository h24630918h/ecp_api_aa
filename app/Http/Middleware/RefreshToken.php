<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;


class RefreshToken extends BaseMiddleware
{
    public function handle($request, Closure $next)
    {

        try {
            $this->checkForToken($request);
            if ($this->auth->parseToken()->authenticate()) {
                $request['token'] = $this->auth->refresh();
                return $next($request);
            }
            return response()->json(['未登入'], 400, [], JSON_UNESCAPED_UNICODE);
        } catch (TokenExpiredException $exception) {
            try {
                $token = $this->auth->refresh();
                Auth::guard('api')->onceUsingId($this->auth->manager()->getPayloadFactory()->buildClaimsCollection()->toPlainArray()['sub']);
            } catch (JWTException $exception) {
                return response()->json(['未登入'], 400, [], JSON_UNESCAPED_UNICODE);
            }
        } catch (TokenBlacklistedException $exception) {
            return response()->json(['未登入'], 400, [], JSON_UNESCAPED_UNICODE);
        }
        $request['token'] = $token;
        return $next($request);
    }
}
