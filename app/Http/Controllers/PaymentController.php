<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PaymentService;
use Validator;
use Illuminate\Validation\Rule;

class PaymentController extends Controller
{
    public $paymentService;

    public function __construct()
    {
        $this->paymentService = new PaymentService();
    }

    //region 新增付款方式

    /**
     * 新增付款方式
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newPayment(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'payment_name' => 'required|max:20|string',
                'payment_flow' => [
                    'required',
                    'between:1,2',
                    'integer',
                    Rule::unique('payments')->where(function ($query) use ($request) {
                        return $query
                            ->where('payment_name', $request->payment_name)
                            ->where('payment_flow', $request->payment_flow);
                    }),

                ],

            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->paymentService->newPayment($postData));
    }
    //endregion

    //region 刪除付款方式

    /**
     * 刪除付款方式
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deletePayment(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'payment_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->paymentService->deletePayment($postData));
    }
    //endregion

    //region 取得全部付款方式
    /**
     * 取得全部付款方式
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllPayment()
    {
        return $this->responseWithJson($this->paymentService->getAllPayment());
    }
    //endregion

    //region 取得商品by id

    /**
     *  取得付款方式by id
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPaymentById(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'payment_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->paymentService->getPaymentById($postData));
    }
    //endregion


}
