<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ItemService;

use Validator;

class ItemController extends Controller
{
    public $itemService;

    public function __construct()
    {
        $this->itemService = new ItemService();
    }


    //region 新增商品

    /**
     * 新增商品
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newItem(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'item_name' => 'required|max:20|string',
                'item_amount' => 'required|integer',
                'unit' => 'required|max:6|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->itemService->newItem($postData));
    }
    //endregion

    //region 刪除商品

    /**
     * 刪除商品
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteItem(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'item_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->itemService->deleteItem($postData));
    }
    //endregion

    //region 取得全部商品
    /**
     * 取得全部會員
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllItem()
    {
        return $this->responseWithJson($this->itemService->getAllItem());
    }
    //endregion

    //region 取得商品by id

    /**
     *  取得商品by id
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getItemById(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'item_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->itemService->getItemById($postData));
    }
    //endregion


}
