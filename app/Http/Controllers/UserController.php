<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Validator;

class UserController extends Controller
{
    public $userService;


    public function __construct()
    {
        $this->userService = new UserService();
    }

    //region 會員註冊

    /**
     * 會員註冊
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                    'unique:users'
                ],
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                    'confirmed'
                ],
                'name' => 'required|max:10|string',
                'phone' => 'required|max:10|string',
                'addr' => 'required|max:200|string',
                'email' => 'required|max:50|string|unique:users',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->userService->register($postData));
    }
    //endregion

    // region 登入

    /**
     * 會員登入
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $postData = $request->only('account', 'password');
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string'
                ],
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string',
                ]
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->userService->login($postData, $request->ip()));

    }
    //endregion


    //region 取得全部會員
    /**
     * 取得全部會員
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllUser()
    {
        return $this->responseWithJson($this->userService->getAllUser());
    }
    //endregion

    //region 取得會員資料 by帳號
    /**
     * 取得會員資料 by帳號
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserByAccount(Request $request)
    {

        $postData = array("account" => $request->account);
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string'
                ],
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->userService->getUserByAccount($postData));
    }
    //endregion


    //region  刪除會員資料

    /**
     *   刪除會員資料
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteUser(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:4,12',
                    'regex:/^[\x7f-\xffA-Za-z0-9]+$/',
                    'string'
                ],
            ]
        );
        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        return $this->responseWithJson($this->userService->deleteUser($postData));


    }
    //endregion

    //region 取得使用者訂單

    /**
     *   取得使用者訂單
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserOrder(Request $request)
    {
        $postData = $request->all();

        return $this->responseWithJson($this->userService->getUserOrder($postData));
    }
    //endregion


    //region 取得個人信用卡資訊

    /**
     * 取得個人信用卡資訊
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserCredits(Request $request)
    {
        $postData = $request->all();
        return $this->responseWithJson($this->userService->getUserCredits($postData));
    }
    //endregion

}


