<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Services\BaseService;
use Validator;

class OrderController extends Controller
{
    use BaseService;

    public $orderService;

    public function __construct()
    {
        $this->orderService = new OrderService();
    }

    //region 新增ecp訂單

    /**
     * 新增ecp訂單
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newECPOrder(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'payment_id' => 'required|integer',
                'description' => 'required|max:200|string',
                'order_detail' => 'required|array',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        #region 陣列內容 驗證
        $odItemJsonArray = array();
        $validArrayRule = array(
            'item_id' => 'required|integer',
            'quantity' => 'required|integer',
        );
        //遍歷整個array 將資料json_decode並驗證，若成功則推入$odItemJsonArray
        foreach ($postData['order_detail'] as $odItem) {
            $odItemJson = json_decode($odItem, true);
            //若json解碼失敗，取出錯誤碼並換成中文錯誤訊息並傳出
            if (!$odItemJson) {
                $result = json_last_error();
                $resultStr = $this->getJson_last_error($result);
                return $this->responseWithJson(array('error' => $resultStr, 'code' => config('apiCode.validateFail')));
            }
            array_push($odItemJsonArray, $odItemJson);
        }

        $odValidator = Validator::make($odItemJson, $validArrayRule);
        if ($odValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        #endregion

        return $this->responseWithJson($this->orderService->newECPOrder($postData, $odItemJsonArray));
    }
    //endregion

    //region 接收ecp訂單回應

    /**
     * 接收ecp訂單回應
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recieveECPResult(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'MerchantID' => 'required|max:10|string',
                'MerchantTradeNo' => 'required|max:20|string',
                'StoreID' => 'required|max:20|string',
                'RtnCode' => 'required|integer',
                'RtnMsg' => 'required|max:200|string',
                'TradeNo' => 'required|max:20|string',
                'TradeAmt' => 'required|integer',
                'PaymentDate' => 'required|string|max:20|date_format:"Y-m-d H:i:s"',
                'PaymentType' => 'required|max:20|string',
                'PaymentTypeChargeFee' => 'required|integer',
                'TradeDate' => 'required|string|max:20|date_format:"Y-m-d H:i:s"',
                'SimulatePaid' => 'required|integer',

                'CustomField1' => 'required|max:50|string',
                'CustomField2' => 'required|max:50|string',
                'CustomField3' => 'required|max:50|string',
                'CustomField4' => 'required|max:50|string',
                'CheckMacValue' => 'required|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->orderService->recieveECPResult($postData));
    }
    //endregion

    //region 查詢ECP訂單

    /**
     * 查詢ECP訂單
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchECPOrder(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'order_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->orderService->searchECPOrder($postData));
    }
    //endregion

    //region取得本機ECP訂單 by id
    /**
     * 取得本機ECP訂單 by id
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getECPOrderById(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'order_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->orderService->getECPOrderById($postData));
    }
    //endregion

    //region 新增 PayByPrime tapPay訂單

    /**
     * region 新增 PayByPrime tapPay訂單
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function PayByPrimeTpOrder(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'payment_id' => 'required|integer',
                'prime' => 'required|max:67|string',
                'details' => 'required|max:100|string',
                'order_detail' => 'required|array',
                'remember' => 'required|boolean',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        #region 陣列內容 驗證
        $odItemJsonArray = array();
        $validArrayRule = array(
            'item_id' => 'required|integer',
            'quantity' => 'required|integer',
        );
        //遍歷整個array 將資料json_decode並驗證，若成功則推入$odItemJsonArray
        foreach ($postData['order_detail'] as $odItem) {
            $odItemJson = json_decode($odItem, true);
            //若json解碼失敗，取出錯誤碼並換成中文錯誤訊息並傳出
            if (!$odItemJson) {
                $result = json_last_error();
                $resultStr = $this->getJson_last_error($result);
                return $this->responseWithJson(array('error' => $resultStr, 'code' => config('apiCode.validateFail')));
            }
            array_push($odItemJsonArray, $odItemJson);
        }

        $odValidator = Validator::make($odItemJson, $validArrayRule);
        if ($odValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        #endregion

        return $this->responseWithJson($this->orderService->PayByPrimeTpOrder($postData, $odItemJsonArray));
    }
    //endregion

    //region 新增 PayByToken tapPay訂單

    /**
     * region 新增 PayByToken tapPay訂單
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function PayByTokenTpOrder(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'payment_id' => 'required|integer',
                'credit_id' => 'required|integer',
                'currency' => 'required|max:3|string',
                'details' => 'required|max:100|string',
                'order_detail' => 'required|array',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        #region 陣列內容 驗證
        $odItemJsonArray = array();
        $validArrayRule = array(
            'item_id' => 'required|integer',
            'quantity' => 'required|integer',
        );
        //遍歷整個array 將資料json_decode並驗證，若成功則推入$odItemJsonArray
        foreach ($postData['order_detail'] as $odItem) {
            $odItemJson = json_decode($odItem, true);
            //若json解碼失敗，取出錯誤碼並換成中文錯誤訊息並傳出
            if (!$odItemJson) {
                $result = json_last_error();
                $resultStr = $this->getJson_last_error($result);
                return $this->responseWithJson(array('error' => $resultStr, 'code' => config('apiCode.validateFail')));
            }
            array_push($odItemJsonArray, $odItemJson);
        }

        $odValidator = Validator::make($odItemJson, $validArrayRule);
        if ($odValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        #endregion

        return $this->responseWithJson($this->orderService->PayByTokenTpOrder($postData, $odItemJsonArray));
    }
    //endregion

    //region 查詢TP訂單

    /**
     *  查詢TP訂單
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchTPOrder(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'start_time' => 'string|max:20|date_format:"Y-m-d H:i:s"|required_with:end_time',
                'end_time' => 'string|max:20|date_format:"Y-m-d H:i:s"|required_with:start_time',
                'upper_limit' => 'integer|required_with:lower_limit',
                'lower_limit' => 'integer|required_with:upper_limit',
                'order_id' => 'integer',
                'page' => 'integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->orderService->searchTPOrder($postData));
    }
    //endregion

    //region 取得本機TP訂單 by id
    /**
     * 取得本機TP訂單 by id
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTPOrderById(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'order_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        return $this->responseWithJson($this->orderService->getTPOrderById($postData));
    }
    //endregion

}



