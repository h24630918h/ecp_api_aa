<?php

namespace App;

use App\Entities\Model\Credit;
use App\Entities\Model\ECPOrder;
use App\Entities\Model\TPOrder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account', 'password', 'name','phone','addr','email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'delete_at'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function ecp_orders()
    {
        return $this->hasMany(ECPOrder::class, 'user_id');
    }
    public function tp_orders()
    {
        return $this->hasMany(TPOrder::class, 'user_id');
    }

    public function credits()
    {
        return $this->hasMany(Credit::class, 'user_id');
    }

}
