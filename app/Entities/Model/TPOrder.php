<?php

namespace App\Entities\Model;

use App\Entities\PlatformModel;

class TPOrder extends PlatformModel
{
    protected $table = 'tp_orders';
    protected $primaryKey = 'order_id';
    protected $fillable = [
        'user_id','payment_id','currency','order_number','bank_transaction_id','details','amount','status','transaction_time',

    ];
    protected $hidden = [
        'delete_at'
    ];
    public function tp_order_details()
    {
        return $this->hasMany(TPOrderDetail::class, 'order_id');
    }
}

