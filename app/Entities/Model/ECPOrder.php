<?php

namespace App\Entities\Model;

use App\Entities\PlatformModel;

class ECPOrder extends PlatformModel
{
    protected $table = 'ecp_orders';
    protected $primaryKey = 'order_id';
    protected $fillable = [
        'user_id', 'trade_no', 'trade_date', 'payment_id', 'total_amount', 'description', 'rtn_code', 'check_mac_value'
    ];
    protected $hidden = [
        'delete_at'
    ];
    public function ecp_order_details()
    {
        return $this->hasMany(ECPOrderDetail::class, 'order_id');
    }
}
