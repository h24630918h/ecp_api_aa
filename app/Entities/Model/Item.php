<?php

namespace App\Entities\Model;

use App\Entities\PlatformModel;

class Item extends PlatformModel
{
    protected $table = 'items';
    protected $primaryKey = 'item_id';
    protected $fillable = [
        'item_name', 'item_amount', 'unit'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
