<?php

namespace App\Entities\Model;

use App\Entities\PlatformModel;

class Request extends PlatformModel
{
    protected $table = 'requests';
    protected $primaryKey = 'req_id';
    protected $fillable = [
        'request', 'code', 'ip', 'url'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
