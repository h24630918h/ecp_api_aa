<?php

namespace App\Entities\Model;

use App\Entities\PlatformModel;

class Payment extends PlatformModel
{
    protected $table = 'payments';
    protected $primaryKey = 'payment_id';
    protected $fillable = [
        'payment_name','payment_flow'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
