<?php


namespace App\Entities\Model;

use App\Entities\PlatformModel;

class Credit extends PlatformModel
{
    protected $table = 'credits';
    protected $primaryKey = 'credit_id';
    protected $fillable = [
        'user_id', 'card_token', 'card_key', 'bin_code', 'last_four', 'issuer', 'issuer_zh_tw',
        'bank_id', 'funding', 'type', 'level', 'country', 'country_code', 'expiry_date'
    ];
    protected $hidden = [
        'card_token', 'card_key', 'delete_at'
    ];
}
