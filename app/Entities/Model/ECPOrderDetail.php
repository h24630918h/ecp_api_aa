<?php

namespace App\Entities\Model;

use App\Entities\PlatformModel;

class ECPOrderDetail extends PlatformModel
{
    protected $table = 'ecp_order_details';
    protected $primaryKey = 'od_id';
    protected $fillable = [
        'order_id', 'item_id', 'quantity'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
