<?php
return [
    'TP_MerchantID' => env('TP_MerchantID', ""),
    'TP_AppID' => env('TP_AppID', ""),
    'TP_AppKey' => env('TP_AppKey', ""),
    'TP_PartnerKey' => env('TP_PartnerKey', ""),
    'TP_Base_Uri' => env('TP_Base_Uri', "")
];
