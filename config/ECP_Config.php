<?php
return [
    'ECP_MerchantID' => env('ECP_MerchantID', ""),
    'ECP_PaymentType' => env('ECP_PaymentType', 'aio'),
    'ECP_ReturnURL' => env('ECP_ReturnURL', ""),
    'ECP_EncryptType' => env('ECP_EncryptType', 1),
    'ECP_HashKey' => env('ECP_HashKey', 1),
    'ECP_HashIV' => env('ECP_HashIV', 1),
    'ECP_Base_Uri' => env('ECP_Base_Uri', "")
];
