<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->Increments('user_id')->comment('PK');
            $table->string('account', 12)->unique()->comment('帳號');
            $table->string('password',60)->comment('密碼');
            $table->string('name',10)->comment('姓名');
            $table->string('phone',10)->comment('電話');
            $table->string('addr',200)->comment('地址');
            $table->string('email',50)->unique()->comment('信箱');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
