<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTpOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tp_order_details', function (Blueprint $table) {
            $table->Increments('od_id')->comment('PK');
            $table->unsignedInteger('order_id')->comment('訂單號');
            $table->unsignedInteger('item_id')->comment('產品號');
            $table->integer('quantity')->comment('數量');

            $table->foreign('order_id')->references('order_id')->on('tp_orders')->onDelete('cascade');
            $table->foreign('item_id')->references('item_id')->on('items')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_order_details');
    }
}
