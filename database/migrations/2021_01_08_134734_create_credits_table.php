<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->Increments('credit_id')->comment('PK');
            $table->unsignedInteger('user_id')->comment('信用卡持有人');
            $table->string('card_token', 64)->comment('卡片識別字串');
            $table->string('card_key', 67)->comment('卡片安全金鑰');

            $table->string('bin_code', 6)->comment('卡片前六碼');
            $table->string('last_four', 4)->comment('卡片後四碼');
            $table->string('issuer', 10)->comment('發卡銀行');
            $table->string('issuer_zh_tw', 10)->comment('發卡銀行中文名稱');
            $table->string('bank_id', 10)->comment('發卡銀行代碼');
            $table->tinyInteger('funding')->comment('卡片類別');
            $table->tinyInteger('type')->comment('卡片種類');
            $table->string('level', 10)->comment('卡片等級');
            $table->string('country', 20)->comment('發卡行國家');
            $table->string('country_code', 10)->comment('發卡行國家碼');
            $table->string('expiry_date', 6)->comment('卡片到期時間');

            $table->unique(['user_id', 'last_four']);
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
