<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //1:綠界 , 2:tap pay
        Schema::create('payments', function (Blueprint $table) {
            $table->Increments('payment_id')->comment('PK');
            $table->string('payment_name', 20)->comment('付款方式');
            $table->tinyInteger('payment_flow')->comment('金流商');
            $table->unique(['payment_name', 'payment_flow']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
