<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTpOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tp_orders', function (Blueprint $table) {
            $table->Increments('order_id')->comment('PK');
            $table->unsignedInteger('user_id')->comment('購買者');
            $table->unsignedInteger('payment_id')->comment('付款方式');
            $table->string('currency', 3)->comment('貨幣種類');
            $table->string('order_number', 50)->unique()->comment('訂單編號');
            $table->string('bank_transaction_id', 40)->unique()->comment('銀行端訂單編號');
            $table->string('details', 100)->comment('訂單描述');
            $table->Integer('amount')->comment('交易總額');
            $table->unsignedInteger('status')->comment('交易狀態');
            $table->dateTime('transaction_time',)->comment('交易時間');

            $table->string('rec_trade_id', 20)->nullable()->comment('由TapPay伺服器產生的交易字串');
            $table->string('auth_code', 6)->nullable()->comment('銀行授權碼');
            $table->dateTime('bank_transaction_start')->nullable()->comment('銀行處理開始時間');
            $table->dateTime('bank_transaction_end')->nullable()->comment('銀行處理結束時間');
            $table->string('bank_result_code', 10)->nullable()->comment('銀行錯誤代碼');
            $table->string('bank_result_msg', 50)->nullable()->comment('銀行錯誤訊息');
            $table->string('payment_url', 200)->nullable()->comment('付款頁面網址');

            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->foreign('payment_id')->references('payment_id')->on('payments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_orders');
    }
}
