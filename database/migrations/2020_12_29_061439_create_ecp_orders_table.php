<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcpOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //rtn_code    1付款成功 ,2:ATM取號成功 ,10100073:CVS/BARCODE取號成功
        Schema::create('ecp_orders', function (Blueprint $table) {
            $table->Increments('order_id')->comment('PK');
            $table->unsignedInteger('user_id')->comment('購買者');
            $table->unsignedInteger('payment_id')->comment('付款方式');
            $table->string('trade_no', 20)->unique()->comment('交易號碼');
            $table->string('ecp_trade_no', 20)->nullable()->comment('綠界交易號碼');
            $table->dateTime('trade_date')->comment('交易日期');
            $table->dateTime('payment_date')->nullable()->comment('付款日期');
            $table->Integer('total_amount')->comment('交易總額');
            $table->string('payment_type_charge_fee',7)->nullable()->comment('手續費');
            $table->string('description', 200)->comment('交易描述');
            $table->unsignedInteger('rtn_code')->comment('交易狀態');
            $table->text('check_mac_value')->comment('檢查碼');

            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->foreign('payment_id')->references('payment_id')->on('payments')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecp_orders');
    }
}
