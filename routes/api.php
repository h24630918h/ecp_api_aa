<?php

use Illuminate\Http\Request;

Route::group(['middleware' => ['displayChinese', 'cors']], function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');

    Route::group(['middleware' => 'jwt_auth'], function () {
        Route::get('getUserByAccount', 'UserController@getUserByAccount');
        Route::get('getAllUser', 'UserController@getAllUser');
        Route::delete('deleteUser', 'UserController@deleteUser');
        Route::get('getUserOrder', 'UserController@getUserOrder');
        Route::get('getUserCredits', 'UserController@getUserCredits');

        Route::post('newPayment', 'PaymentController@newPayment');
        Route::delete('deletePayment', 'PaymentController@deletePayment');
        Route::get('getAllPayment', 'PaymentController@getAllPayment');
        Route::get('getPaymentById', 'PaymentController@getPaymentById');

        Route::post('newItem', 'ItemController@newItem');
        Route::delete('deleteItem', 'ItemController@deleteItem');
        Route::get('getAllItem', 'ItemController@getAllItem');
        Route::get('getItemById', 'ItemController@getItemById');


        Route::group(['middleware' => 'logAfterRequest'], function () {
            //綠界
            Route::post('newECPOrder', 'OrderController@newECPOrder');
            Route::post('recieveECPResult', 'OrderController@recieveECPResult');
            Route::get('searchECPOrder', 'OrderController@searchECPOrder');
            Route::get('getECPOrderById', 'OrderController@getECPOrderById');

            //tap pay
            Route::post('PayByPrimeTpOrder', 'OrderController@PayByPrimeTpOrder');
            Route::post('PayByTokenTpOrder', 'OrderController@PayByTokenTpOrder');
            Route::get('searchTPOrder', 'OrderController@searchTPOrder');
            Route::get('getTPOrderById', 'OrderController@getTPOrderById');
        });
    });
});

